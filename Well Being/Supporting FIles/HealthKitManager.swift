//
//  HealthKitManager.swift
//  Well Being
//
//  Created by Mahad on 3/3/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import Foundation
import UIKit
import HealthKit

class HealthKitManager {
    
    let healthStore = HKHealthStore()
    
     func authorizeHealthKit( completion: @escaping (Bool) -> ()) {
        var isEnabled = true
        
        if HKHealthStore.isHealthDataAvailable()
        {
//            let stepCount = NSSet(objects: HKQuantityType.quantityType(forIdentifier: .stepCount))
            let healthKitTypesToRead: Set<HKObjectType> = [HKObjectType.quantityType(forIdentifier: .stepCount)!,
                                                           HKObjectType.characteristicType(forIdentifier: .bloodType)!]
            
            healthStore.requestAuthorization(toShare: nil, read: healthKitTypesToRead){(success,error) -> Void in
                isEnabled = success
                completion(isEnabled)
            }
        }
        else
        {
            completion(false)
        }
    }
    
    func readProfile() {
//        var bloodType:HKObjectType?
//
//        do{
//            bloodType = try healthStore.
//        }catch{}
//
//        return bloodType
        
        
//        let stepsCount = HKQuantityType.quantityType(
//            forIdentifier: HKQuantityTypeIdentifier.stepCount)!
//
//        let stepsSampleQuery = HKSampleQuery(sampleType: stepsCount,
//            predicate: nil,
//            limit: 100,
//            sortDescriptors: nil)
//            { [unowned self] (query, results, error) in
//                if let results = results as? [HKQuantitySample] {
//                    print(results)
//                }
//        }
//
//        // Don't forget to execute the Query!
//        healthStore.execute(stepsSampleQuery)
        
        
        
        let stepsCount1 = HKQuantityType.quantityType(
            forIdentifier: HKQuantityTypeIdentifier.stepCount)!
         
        let sumOption = HKStatisticsOptions.cumulativeSum
         
        let statisticsSumQuery = HKStatisticsQuery(quantityType: stepsCount1, quantitySamplePredicate: nil,
            options: sumOption)
            { [unowned self] (query, result, error) in
                if let sumQuantity = result?.sumQuantity() {
    
                    let numberOfSteps = Int(sumQuantity.doubleValue(for: .count()))
                    print(numberOfSteps)
                    //headerView.textLabel.text = "\(numberOfSteps) total"
                    //self.tableView.tableHeaderView = headerView
                }
                //self.activityIndicator.stopAnimating()
         
        }
         
        // Don't forget to execute the query!
        healthStore.execute(statisticsSumQuery)
        
        
    }
    
    func retrieveStepCount(completion: (_ stepRetrieved: Double) -> Void) {

        //   Define the Step Quantity Type
        let stepsCount = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)

        //   Get the start of the day
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)

        //  Set the Predicates & Interval
        let predicate = HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
        var interval = DateComponents()
        interval.day = 1

        //  Perform the Query
        let query = HKStatisticsCollectionQuery(quantityType: stepsCount!, quantitySamplePredicate: predicate, options: [.cumulativeSum], anchorDate: newDate as Date, intervalComponents:interval)

        query.initialResultsHandler = { query, results, error in

            if error != nil {

                //  Something went Wrong
                return
            }
            var dateComponents = DateComponents()
            dateComponents.setValue(-1, for: .day) // -1 day
                   
            let yesterday = Calendar.current.date(byAdding: dateComponents, to: Date())
            if let myResults = results{
                myResults.enumerateStatistics(from: yesterday!, to: date) {
                    statistics, stop in

                    if let quantity = statistics.sumQuantity() {

                        let steps = quantity.doubleValue(for: HKUnit.count())

                        print("Steps = \(steps)")
//                        completion(steps)

                    }
                }
            }


        }

        healthStore.execute(query)
    }
    
    
}

