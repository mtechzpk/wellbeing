//
//  AttributedStringLabel.swift
//  Well Being
//
//  Created by Mahad on 3/31/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit

@IBDesignable class AttributedStringLabel: UILabel {

    @IBInspectable open var firstText: String = ""
    let secondText: String = " *"

    let firstColor: UIColor = UIColor.black
    let secondColor: UIColor = UIColor.red

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let firstTextAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: firstColor]
        
        let secondTextAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: secondColor]
        
        let firstattrString:NSMutableAttributedString = NSMutableAttributedString(string: firstText, attributes: firstTextAttributes)
        
        let secondattrString:NSMutableAttributedString = NSMutableAttributedString(string: secondText, attributes: secondTextAttributes)
        
        firstattrString.append(secondattrString)
        self.attributedText = firstattrString
            
        
//        let attrString :NSMutableAttributedString  = NSAttributedString([![string][1]][1]: firstText) as! NSMutableAttributedString
//
//
//
//
//
//        attrString.addAttribute(NSForegroundColorAttributeName, value: firstColor! as UIColor, range: NSMakeRange(0, 5))
//        attrString.addAttribute(NSForegroundColorAttributeName, value: secondColor! as UIColor, range: NSMakeRange(5, 6))
//        attrString.addAttribute(NSForegroundColorAttributeName, value: thirdColor! as UIColor, range: NSMakeRange(11, 15))
//        self.attributedText = attrString

    }


}

@IBDesignable class BorderView :UIView{
    
    override func awakeFromNib() {
        self.layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1.0
    }
}

@IBDesignable class BorderButton :UIButton{
    
    override func awakeFromNib() {
        self.layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1.0
    }
}

