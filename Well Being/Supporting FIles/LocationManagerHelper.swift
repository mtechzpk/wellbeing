//
//  LocationManagerHelper.swift
//  Well Being
//
//  Created by Mahad on 3/28/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationManagerDelegate:class
{
    func locationUpdated(location:CLLocation)
    func locationUpdateFailed(error:String)
    func locationAuthorizationGranted(granted:Bool)
}


class LocationManagerHelper: NSObject,CLLocationManagerDelegate {
    static let sharedLocationManager:LocationManagerHelper = LocationManagerHelper()
    
    weak var delegate:LocationManagerDelegate?
    private let locationManager = CLLocationManager()
    
    private var isUpdating:Bool = false
    
    private override init()
    {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.activityType = .other
        locationManager.pausesLocationUpdatesAutomatically = false
    }
    
    func getCurrentLocation()
    {
        if (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
        {
            if (!isUpdating)
            {
                self.delegate?.locationAuthorizationGranted(granted: true)
                isUpdating = true
            }
            locationManager.startUpdatingLocation()
        }
        else if (CLLocationManager.authorizationStatus() == .notDetermined)
        {
            locationManager.requestWhenInUseAuthorization()
        }
        else
        {
            self.failedToUpdateLocation(error: "Location usage not allowed. Please allow ALS to acccess your location to see the jobs in your area")
        }
    }
    
    func failedToUpdateLocation(error:String)
    {
       // self.locationManager.stopUpdatingLocation()
        self.delegate?.locationUpdateFailed(error: error)
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedWhenInUse || status == .authorizedAlways)
        {
            if (!isUpdating)
            {
                self.delegate?.locationAuthorizationGranted(granted: true)
                locationManager.startUpdatingLocation()
                isUpdating = true
            }
        }
        else if (status == .denied || status == .restricted)
        {
            self.failedToUpdateLocation(error: "Location usage not allowed. Please allow ALS to acccess your location to see the jobs in your area")
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        if (location.horizontalAccuracy > 100  || location.horizontalAccuracy < 0)
        {
            print("Accuracy not good enough")
            return
        }
//        self.locationManager.stopUpdatingLocation()
        self.delegate?.locationUpdated(location: location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to get Location. Error: \(error.localizedDescription)")
        self.failedToUpdateLocation(error: error.localizedDescription)
    }
}
