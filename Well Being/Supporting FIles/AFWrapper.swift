//
//  AFWrapper.swift
//  Well Being
//
//  Created by Mahad on 3/16/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
import Alamofire

class AFWrapper: NSObject {
    
    class func requestGETURL(_ strURL: String, params : [String : AnyObject]?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void) {
        
        let headers: HTTPHeaders = [
            
            "Accept": "application/json"
        ]
        
        let url = baseURL + strURL
        
        AF.request(url, method: .get , encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler:{ (responseObject) -> Void in
            
            //            print(responseObject)
            
            switch responseObject.result
            {
            case .success(let value):
                do{
                    let data1 =  try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted)
                    success(data1)
                }
                catch
                {
                    
                }
            case .failure(let error):
                failure(error)
            }
        })
    }
    
    
    class func requestPOSTURL(_ strURL : String, params : [String : Any]?, success:@escaping (Data) -> Void, failure:@escaping (Error) -> Void){
        
        //        AF.request(strURL, method: .post, parameters: params, encoder: JSONEncoding.default, headers: headers).resp
        
        let header: HTTPHeaders = [
            
            "Accept": "application/json"
        ]
        
        let url = baseURL + strURL
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (responseObject) -> Void in
            
            //            print(responseObject)
            switch responseObject.result
            {
            case .success(let value):
                do{
                    let data1 =  try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted)
                    success(data1)
                }
                catch
                {
                    
                }
            case.failure(let error):
                failure(error)
            }
            
        })
    }
}
