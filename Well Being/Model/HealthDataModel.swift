//
//  HealthDataModel.swift
//  Well Being
//
//  Created by Mahad on 3/19/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import Foundation

struct HealthDataModel {
    var steps:String? = "0"
    var activeEnergy:String? = ""
    var restingEnergy:String? = ""
    var exerciseMins:String? = ""
    var standMins:String? = ""
    var distance:String? = "0"
    var heartrate:String? = "0"
    var restingheartrate:String? = "0"
    var walkingheartrate:String? = "0"
    var heartratevariablity:String? = "0"
    var flightclimb:String? = "0"
    var sleepAnalysis:String? = ""
    var workouts:String? = ""
    var height:String? = ""
    var weight:String? = ""
    var bodyfatpercentge:String? = ""
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
}
