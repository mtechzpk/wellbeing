// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let questionModel = try? newJSONDecoder().decode(QuestionModel.self, from: jsonData)

import Foundation

// MARK: - QuestionModel
struct QuestionModel: Codable {
    var status: Int?
    var message: String?
    var data: [QuestionsData]?
}

// MARK: - Datum
struct QuestionsData: Codable {
    var id: Int?
    var question: String?
}
