// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let userModel = try? newJSONDecoder().decode(UserModel.self, from: jsonData)

import Foundation

// MARK: - UserModel
struct UserModel: Codable {
    var status: Int?
    var message: String?
    var data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    var id: Int?
    var fname: String?
    var lname: String?
    var email: String?
    var emailVerifiedAt, workEmail: String?
    var homePostcode, workPostcode, age, sex: String?
    var isSmooker, jobTitle, checkDate, createdAt: String?
    var salary: String?
    var height:String?
    var weight:String?
    

    enum CodingKeys: String, CodingKey {
        case id, fname, lname, email
        case emailVerifiedAt = "email_verified_at"
        case workEmail = "work_email"
        case homePostcode = "home_postcode"
        case workPostcode = "work_postcode"
        case age, sex
        case isSmooker = "is_smooker"
        case jobTitle = "job_title"
        case salary,height,weight
        case checkDate = "check_date"
        case createdAt = "created_at"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
