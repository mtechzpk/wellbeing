// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let allRecordsModel = try? newJSONDecoder().decode(AllRecordsModel.self, from: jsonData)

import Foundation

// MARK: - AllRecordsModel
struct AllRecordsModel: Codable {
    var status: Int?
    var message: String?
    var data: [recordData]?
}

// MARK: - Datum
struct recordData: Codable {
    var id: Int?
    var userID, question1ID, question3ID, question4ID, question2ID: String?
    var answer2, answer1, answer3: String?
    var answer4, answerDate, question1, question2: String?
    var question3, question4: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case question1ID = "question1_id"
        case answer1
        case question2ID = "question2_id"
        case answer2
        case question3ID = "question3_id"
        case answer3
        case question4ID = "question4_id"
        case answer4
        case answerDate = "answer_date"
        case question1, question2, question3, question4
    }
}
