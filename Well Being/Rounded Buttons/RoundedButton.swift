//
//  RoundedButton.swift
//  Well Being
//
//  Created by Waqas on 21/02/2020.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    required public init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        self.layer.cornerRadius = 15
    }

}
