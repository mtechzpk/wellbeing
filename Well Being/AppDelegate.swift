//
//  AppDelegate.swift
//  Well Being
//
//  Created by Waqas on 21/02/2020.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
import FacebookCore
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    let notifCenter = UNUserNotificationCenter.current()
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        requestAuthForLocalNotifications()
//        UIApplication.shared.applicationIconBadgeNumber = 0
        IQKeyboardManager.shared().isEnabled = true
        
//        if(launchOptions?[UIApplication.LaunchOptionsKey.localNotification] != nil){
//            // your code here
//            UserDefaults.standard.set(true, forKey: "launchFromNotification")
//        }
//        else
//        {
//            UserDefaults.standard.set(false, forKey: "launchFromNotification")
//        }
        
//        if (launchOptions![UIApplication.LaunchOptionsKey.localNotification] != nil)
//        {
//
//            var notification =
//                launchOptions![UIApplication.LaunchOptionsKey.localNotification]
//        }
        return true
    }

    func requestAuthForLocalNotifications() {
        notifCenter.delegate = self
        notifCenter.requestAuthorization(options: [.alert, .sound,.badge]) { (granted, error) in
            if error != nil {
                // Something went wrong
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions.init(arrayLiteral: [.alert, .badge]))
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.trigger is UNCalendarNotificationTrigger {
            print("remote notification");
            UserDefaults.standard.set(true, forKey: "launchFromNotification")
        }
        
        let userInfo = response.notification.request.content.userInfo
        let title = response.notification.request.content.title
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SecondTypeNotification"), object: title, userInfo: userInfo)
        
        completionHandler()
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0;
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

