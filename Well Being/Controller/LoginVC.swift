//
//  LoginVC.swift
//  Well Being
//
//  Created by Mahad on 3/16/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    // MARK: - Properties
    
    // MARK: - IBOutlets
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var passTxt: UITextField!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissloginView(gesture:)))
//        slideDown.direction = .right
//        view.addGestureRecognizer(slideDown)
        #if DEBUG
        emailTxt.text = "m.haq70@gmail.com"
        passTxt.text = "123"
        #else
//            phoneVerification()
        #endif
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Set up
    // MARK: - IBActions
    
    @IBAction func backBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitBtn_Click(_ sender: Any) {
        if emailTxt.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Email is Empty", Sender: self)
            return
        }
        if passTxt.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Password is Empty", Sender: self)
            return
        }
        
        DoLogin()
        
    }
    
    // MARK: - Navigation
    
    @objc func dismissloginView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Network Manager calls
    
    func DoLogin() {
        
        var parameters = [String:Any]()
        parameters["email"] = emailTxt.text!
        parameters["password"] = passTxt.text!
        
        
        AFWrapper.requestPOSTURL("login", params: parameters, success: { (jsonData) in
            
            do{
                let decoder = JSONDecoder()
                let userModel = try decoder.decode(UserModel.self, from: jsonData)
                if userModel.status == 200
                {
                    //                    AlertHelper.showSuccessAlert(WithTitle: "Success", Message: userModel.message ?? "You Registered Successfully!", Sender: self)
                    //firstInformativeScreenVC
                    userData = userModel.data
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(userData) {
                        let defaults = UserDefaults.standard
                        defaults.set(encoded, forKey: "userData")
                        defaults.set(true, forKey: "isLogined")
                        defaults.synchronize()
                    }
                    
                    let firstInformativeScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstInformativeScreenVC") as! FirstInformativeScreenVC
                    self.navigationController?.pushViewController(firstInformativeScreenVC, animated: true)
                }
                else
                {
                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: userModel.message ?? "Failed to Registered", Sender: self)
                }
            }
            catch
            {
                
            }
            
            
            print(jsonData)
            
        }, failure: { (Error) in
            print(Error.localizedDescription)
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: Error.localizedDescription, Sender: self)
        })
    }
    
    // MARK: - Extensions
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
