//
//  SecondInfoScreenVC.swift
//  Well Being
//
//  Created by Mahad on 3/16/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit

class SecondInfoScreenVC: UIViewController {

    // MARK: - Properties
    // MARK: - IBOutlets
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

//        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismisssecondInfoView(gesture:)))
//        slideDown.direction = .right
//        view.addGestureRecognizer(slideDown)
        // Do any additional setup after loading the view.
    }
    // MARK: - Set up
    // MARK: - IBActions
    
    @IBAction func backBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func nextBtn_Click(_ sender: Any) {
        let questionVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
        self.navigationController?.pushViewController(questionVC, animated: true)
    }
    
    @IBAction func allTaskBtn_Click(_ sender: Any) {
        let calenderVC = self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        
//        calenderVC.answer1 = self.ans1
//        calenderVC.answer2 = self.ans2
//        calenderVC.answer3 = self.ans3
//        calenderVC.answer4 = self.ans4
        
        self.navigationController?.pushViewController(calenderVC, animated: true)
    }
    
    
    
    // MARK: - Navigation
    @objc func dismisssecondInfoView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Network Manager calls
    // MARK: - Extensions
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
