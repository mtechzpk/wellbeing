//
//  ViewController.swift
//  Well Being
//
//  Created by Waqas on 21/02/2020.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
//import FacebookCore
//import FacebookLogin

class ViewController: UIViewController {

    //@IBOutlet weak var FacebookLoginButton: UIButton!
    let userDefaults:UserDefaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self,selector: #selector(SecondTypeNotification),
                                name: NSNotification.Name(rawValue: "SecondTypeNotification"),
                                object: nil)
        
        checkLoginStatus()
//        if(UserDefaults.standard.bool(forKey: "launchFromNotification") == true)
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: QuestionVC = storyboard.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
        // Do any additional setup after loading the view.
    }
    
   
    override func viewDidAppear(_ animated: Bool) {
       // CheckingLoginStatus()
//        if(UserDefaults.standard.bool(forKey: "launchFromNotification") == true)
//        {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let vc: QuestionVC = storyboard.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
//            self.navigationController?.pushViewController(vc, animated: true)
//            UserDefaults.standard.set(false, forKey: "launchFromNotification")
//        }
    }
    
    func checkLoginStatus() {
        if let islogin = userDefaults.value(forKey: "isLogined") as? Bool
                {
                    if (islogin)
                    {
                        if let savedPerson = userDefaults.object(forKey: "userData") as? Data {
                            let decoder = JSONDecoder()
                            if let loadedPerson = try? decoder.decode(DataClass.self, from: savedPerson) {
                                print(loadedPerson)
                                userData = loadedPerson
                            }
                        }
                        
                        
        //                let dictionarydata:Data = userDefaults.object(forKey: "userData") as! Data
        //                let userData = NSKeyedUnarchiver.unarchiveObject(with: dictionarydata) as! UserModel
        //                userModel = userData
                        let firstInformativeScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "FirstInformativeScreenVC") as! FirstInformativeScreenVC
                        self.navigationController?.pushViewController(firstInformativeScreenVC, animated: true)
                        
                    }
                }
    }
    
    
    @objc func SecondTypeNotification(notification: NSNotification){
        DispatchQueue.main.async
          {
            //Land on SecondViewController
            if let islogin = self.userDefaults.value(forKey: "isLogined") as? Bool
                    {
                        if (islogin)
                        {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc: QuestionVC = storyboard.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
                            self.navigationController?.pushViewController(vc, animated: true)
                            
                        }
                    }
            UserDefaults.standard.set(false, forKey: "launchFromNotification")
          }
      }
    
    func CheckingLoginStatus() {
        if UserDefaults.standard.bool(forKey: "LoginStatus") == true{
            self.performSegue(withIdentifier: "FirstInformationSegue", sender: self)
        }
    }
    
    
    @IBAction func signupBtn_Click(_ sender: Any) {
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    @IBAction func signinBtn_Click(_ sender: Any) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
//    @IBAction func FacebookLoginButton(_ sender: Any) {
//        let loginManager=LoginManager()
//        loginManager.logIn(permissions: [.publicProfile], viewController : self) { loginResult in
//            switch loginResult {
//            case .failed(let error):
//                print(error)
//            case .cancelled:
//                print("User cancelled login")
//            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
//                print("Logged in")
//
//                UserDefaults.standard.set(true, forKey: "LoginStatus")
//
//                print(accessToken.tokenString)
//
//                UserDefaults.standard.set(accessToken.userID, forKey: "UserId")
//                if accessToken.userID == UserDefaults.standard.string(forKey: "UserId"){
//                    self.performSegue(withIdentifier: "FirstInformationSegue", sender: nil)
//                }
//            }
//        }
//    }

}

