//
//  CalenderVC.swift
//  Well Being
//
//  Created by Waqas on 26/02/2020.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
import JTAppleCalendar
import VACalendar


class CalenderVC: UIViewController {

    @IBOutlet weak var CalenfderCollection: JTACMonthView!
    let viewA = UIView()
    
    
    @IBOutlet var monthHeaderView: VAMonthHeaderView!{
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "LLLL yyyy"
            let appereance = VAMonthHeaderViewAppearance(
                monthTextWidth: 200,
                previousButtonImage: #imageLiteral(resourceName: "previous"),
                nextButtonImage: #imageLiteral(resourceName: "next"),
                dateFormatter: dateFormatter
            )
            monthHeaderView.delegate = self
            monthHeaderView.backgroundColor = UIColor.clear
//            monthHeaderView.tintColor = UIColor.black
            monthHeaderView.appearance = appereance
            monthHeaderView.backgroundColor = UIColor.clear
        }
    }
    //arrowtriangle.left.fill
    
    @IBOutlet var weekDaysView: VAWeekDaysView!{
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, calendar: defaultCalendar)
            weekDaysView.appearance = appereance
        }
    }
    
    
    var calendarView: VACalendarView!
    
    @IBOutlet var calendarVw: UIView!
    
    @IBOutlet var calendarMainView: UIView!
    
    
    
//    @IBOutlet var calendarView: VACalendarView!
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    
    }()
    
    
    
    @IBOutlet var feelingemojiView: UIImageView!
    @IBOutlet var feelingemojiLbl: UILabel!
    @IBOutlet var proemojiView: UIImageView!
    @IBOutlet var proemojiLbl: UILabel!
    
    @IBOutlet var sleptemojiView: UIImageView!
    @IBOutlet var sleptemojiLbl: UILabel!
    
    @IBOutlet var stressemojiView: UIImageView!
    @IBOutlet var stressemojiLbl: UILabel!
    
    @IBOutlet var ansStackView: UIStackView!
    @IBOutlet var ansStackView1: UIStackView!
    
    @IBOutlet var EditBtn: UIButton!
    
    
//    @IBOutlet var btntoCalendarConstraint: NSLayoutConstraint!
    
    @IBOutlet var btntoStackVwConstraint: NSLayoutConstraint!
    
    var answer1 = "5"
    var answer2 = "5"
    var answer3 = "5"
    var answer4 = "5"
    
    
    var recordarray:[recordData] = []
    var selectedrecord:recordData?
    let dateformatter = DateFormatter()
    
    var selectedDate:String?
    var lastSelectedDate:Date = Date()
    var signupDate:Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        CalenfderCollection.calendarDelegate = self
//        CalenfderCollection.calendarDataSource = self
        viewA.translatesAutoresizingMaskIntoConstraints = true
        
        
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
        calendarView.showDaysOut = true
        calendarView.selectionStyle = .single
        calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal
        calendarMainView.addSubview(calendarView)
//        calendarVw.addSubview(calendarView)
        print(calendarView.startDate)
        
        
        
        
        
//        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismisscalendarView(gesture:)))
//        slideDown.direction = .right
//        view.addGestureRecognizer(slideDown)
    }
    
    @objc func dismisscalendarView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        dateformatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        dateformatter.dateFormat = "yyyy-MM-dd"
        
        signupDate = dateformatter.date(from: String(userData!.createdAt!.split(separator: " ")[0]))!
        
        
        feelingemojiLbl.text = answer1+"/10"
        proemojiLbl.text = answer2+"/10"
        sleptemojiLbl.text = answer3+"/10"
        stressemojiLbl.text = answer4+"/10"
        
        feelingemojiView.image = UIImage(named: "Emoji\(answer1)")
        proemojiView.image = UIImage(named: "p\(answer2)")
        sleptemojiView.image = UIImage(named: "slept\(answer3)")
        stressemojiView.image = UIImage(named: "stress\(answer4)")
        
        
        GetDataList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if calendarView.frame == .zero {
            print(calendarMainView.frame)
            calendarView.frame = CGRect(
                x: 0,
                y: weekDaysView.frame.maxY,
                width: view.frame.width,
                height: calendarMainView.frame.height - weekDaysView.frame.maxY
            )
            calendarView.setup()
        }
    }
    
    
//    override func viewWillLayoutSubviews() {
//        <#code#>
//    }
    
    @IBAction func editBtn_Click(_ sender: Any) {
        if(selectedrecord == nil)
        {
            let calendar = Calendar.current
            
            let dateComponents = calendar.dateComponents([.hour], from: Date())
            
            if(dateComponents.hour! < 18)
            {
                print("abc")
                AlertHelper.showErrorAlert(WithTitle: "Alert!", Message: "You are not Allowed to add Data before 6 pm", Sender: self)
                return
            }
            
        }
        
        
        let questionVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
        questionVC.record = selectedrecord
        questionVC.answerdate = selectedDate ?? ""
        questionVC.iseditMode = selectedrecord == nil ? false:true
        self.navigationController?.pushViewController(questionVC, animated: true)
    }
    
    @IBAction func backBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
//    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
//        <#code#>
//    }
    
    func selectedDate(_ date: Date) {
        
        if(date > Date() || date < signupDate)
        {
//            calendarView.selectDates(date())
            return
        }
        
        
        lastSelectedDate = date
        let record = recordarray.filter{$0.answerDate == dateformatter.string(from: date)}.first
        selectedrecord = record
        dateformatter.dateFormat = "YYYY-MM-dd"
        selectedDate = dateformatter.string(from: date)
        if record != nil
        {
            feelingemojiLbl.text = record!.answer1! + "/10"
            proemojiLbl.text = record!.answer2!+"/10"
            sleptemojiLbl.text = record!.answer3!+"/10"
            stressemojiLbl.text = record!.answer4!+"/10"
            
            feelingemojiView.image = UIImage(named: "Emoji\(record!.answer1!)")
            proemojiView.image = UIImage(named: "p\(record!.answer2!)")
            sleptemojiView.image = UIImage(named: "slept\(record!.answer3!)")
            stressemojiView.image = UIImage(named: "stress\(record!.answer4!)")
            ansStackView.isHidden = false
            ansStackView1.isHidden = false
            EditBtn.isHidden = false
            EditBtn.setTitle("Update", for: .normal)
//            btntoCalendarConstraint.isActive = false
//            btntoStackVwConstraint.isActive = true
        }
        else
        {
            ansStackView.isHidden = true
            ansStackView1.isHidden = true
            EditBtn.setTitle("Add", for: .normal)
            
//            btntoCalendarConstraint.isActive = true
//            btntoStackVwConstraint.isActive = false
//            EditBtn.isHidden = true
        }
    }
    
    
    
    
    

    func configureCell(view: JTACDayCell?, cellState: CellState) {
       guard let cell = view as? CallenderCollectionCell  else { return }
       cell.DateLabel.text = cellState.text
       handleCellTextColor(cell: cell, cellState: cellState)
    }
        
    func handleCellTextColor(cell: CallenderCollectionCell, cellState: CellState) {
       if cellState.dateBelongsTo == .thisMonth {
          cell.DateLabel.textColor = UIColor.black
       } else {
          cell.DateLabel.textColor = UIColor.gray
       }
    }

    
    func abc() {
        //UIView.an
    }

}
//extension CalenderVC: JTACMonthViewDataSource {
//    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy MM dd"
//        let startDate = formatter.date(from: "2018 01 01")!
//        let endDate = Date()
//        return ConfigurationParameters(startDate: startDate, endDate: endDate)
//    }
//}

//extension CalenderVC: JTACMonthViewDelegate {
//    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
//        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! CallenderCollectionCell
//        cell.DateLabel.text = cellState.text
//        return cell
//    }
//    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
//        let cell = cell as! CallenderCollectionCell
//        cell.DateLabel.text = cellState.text
//    }
//}


extension CalenderVC: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
    
}


extension CalenderVC: VADayViewAppearanceDelegate {
    
    func backgroundColor(for state: VADayState) -> UIColor {
        return.clear
    }
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .black
        case .unavailable:
            return .darkGray
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return .green
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -2
        }
    }
    
}



extension CalenderVC: VACalendarViewDelegate {
    
    func selectedDates(_ dates: [Date]) {
        
        let date = dates.first ?? Date()
        
        if(date > Date() || date < signupDate)
                {
                    calendarView.selectDates([lastSelectedDate])
//                    selectedDates([Date()])
                    return
                }
        
        calendarView.startDate = dates.last ?? Date()
        print(dates)
    }
}

extension CalenderVC: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}

extension CalenderVC{
    func GetDataList() {
        
        var parameters = [String:Any]()
        parameters["user_id"] = userData?.id!
        
        
        AFWrapper.requestPOSTURL("fetch_user_data", params: parameters, success: { (jsonData) in
            
            do{
                let decoder = JSONDecoder()
                let allRecord = try decoder.decode(AllRecordsModel.self, from: jsonData)
                if allRecord.status == 200
                {

                    self.recordarray = allRecord.data ?? []
                    
                }
                else
                {
                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: allRecord.message ?? "Failed to Get Data", Sender: self)
                }
                self.reloadCalendarData()
            }
            catch
            {
                
            }
            
            
            print(jsonData)
            
        }, failure: { (Error) in
            print(Error.localizedDescription)
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: Error.localizedDescription, Sender: self)
        })
    }
    
    func reloadCalendarData() {
        var date = [(Date, [VADaySupplementary])]()

        
        for obj in recordarray
        {
            let objdate:Date = dateformatter.date(from: obj.answerDate!)!
            date.append((objdate,[VADaySupplementary.bottomDots([.green])]))
        }
        
        calendarView.setSupplementaries(date)
        
        
        selectedDate(calendarView.startDate)
        textBackgroundColor(for: .selected)
        
//        calendarView.setup()
    }

}
