//
//  FirstInformativeScreenVC.swift
//  Well Being
//
//  Created by Waqas on 25/02/2020.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
import HealthKit
import CoreLocation

class FirstInformativeScreenVC: UIViewController {
    
    // MARK: - Properties
    
    var healthStore = HKHealthStore()
    let healthKitManagerObj = HealthKitManager()
    
    var isDataOpen:Bool = false
    
//    var latitude:Double = 0.0
//    var longitude:Double = 0.0
    var healthdata = HealthDataModel()
    
    var locationManager:LocationManagerHelper?
    
    //    let pedometer = CMPedometer()
    
    var installDate: Date? {
        guard
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last,
            let attributes = try? FileManager.default.attributesOfItem(atPath: documentsURL.path)
            else { return nil }
        return attributes[.creationDate] as? Date
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var healthStackVw: UIStackView!
    
    @IBOutlet var healthtoBtnConstraint: NSLayoutConstraint!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getQuestions()
//        checkPermission()
        let tap = UITapGestureRecognizer(target: self, action: #selector(stackViewTapped))
        healthStackVw.addGestureRecognizer(tap)
        
        self.showNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getQuestions()
        //        willEnterForeground()
        
        self.locationManager = LocationManagerHelper.sharedLocationManager
        self.locationManager!.delegate = self
        self.locationManager!.getCurrentLocation()
        checkPermission()

        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(UserDefaults.standard.bool(forKey: "launchFromNotification") == true)
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: QuestionVC = storyboard.instantiateViewController(withIdentifier: "QuestionVC") as! QuestionVC
            self.navigationController?.pushViewController(vc, animated: true)
            UserDefaults.standard.set(false, forKey: "launchFromNotification")
        }
    }
    
    // MARK: - Set up
    
    func showNotification() {
        
        
        
        let content = UNMutableNotificationContent()
        content.title = "Please complete today's survey"
        content.body = ""
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        // You can use the user info array if you need to include additional information in your local notification.
        // Then you could use that additional information to perform any kind of action when the notification is opened by the user
        content.userInfo = ["CustomData": "You will be able to include any kind of information here"]
        
        let yourDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())!
        //        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: yourDate.timeIntervalSinceNow, repeats: false)
        var dateComponents = DateComponents()
        dateComponents.hour = 21
        dateComponents.minute = 00
        
        
        let trigger = UNCalendarNotificationTrigger.init(dateMatching: dateComponents, repeats: true)
        
        let request = UNNotificationRequest.init(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request)
        
    }
    
    //    @objc func willEnterForeground() {
    //        healthtoBtnConstraint.isActive = true
    //        datatoBtnConstraint.isActive = false
    //        datastackVw.isHidden = true
    //        isDataOpen = false
    //        print("will enter foreground")
    //    }
    
    @objc func stackViewTapped(gesture: UISwipeGestureRecognizer) {
        
        let healthDataVC = self.storyboard?.instantiateViewController(withIdentifier: "HealthDataVC") as! HealthDataVC
        healthDataVC.datamodal = healthdata
        self.navigationController?.pushViewController(healthDataVC, animated: true)
    }
    // MARK: - IBActions
    
    @IBAction func logoutBtn_Click(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: "userData")
        defaults.set(false, forKey: "isLogined")
        defaults.synchronize()
        self.navigationController?.popToRootViewController(animated: true)
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    @IBAction func nextBtn_Click(_ sender: Any) {
        //        let secondInfoScreenVC = self.storyboard?.instantiateViewController(withIdentifier: "SecondInfoScreenVC") as! SecondInfoScreenVC
        //        self.navigationController?.pushViewController(secondInfoScreenVC, animated: true)
        
        let calenderVC = self.storyboard?.instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        self.navigationController?.pushViewController(calenderVC, animated: true)
    }
    
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    
    
    
    func getQuestions() {
        
        AFWrapper.requestGETURL("get_questions", params: nil, success: { (jsonData) in
            do{
                let decoder = JSONDecoder()
                let questionModel = try decoder.decode(QuestionModel.self, from: jsonData)
                if questionModel.status == 200
                {
                    questionsData = questionModel.data
                }
                else
                {
                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: questionModel.message ?? "Failed to getQ", Sender: self)
                }
            }
            catch{
                
            }
        }) { (Error) in
            print(Error.localizedDescription)
        }
        
    }
    
    
    
}
// MARK: - Extensions

extension FirstInformativeScreenVC:LocationManagerDelegate{
    func locationUpdated(location: CLLocation) {
        print("Updated")
        self.healthdata.latitude = location.coordinate.latitude
        self.healthdata.longitude = location.coordinate.longitude
        
    }
    
    func locationUpdateFailed(error: String) {
        print("Failed")
    }
    
    func locationAuthorizationGranted(granted: Bool) {
        print("Granted")
    }
    
    
}


extension FirstInformativeScreenVC {
    
    func checkPermission() {
        
        //Activity
        let flightsClimbed:HKQuantityType? = HKObjectType.quantityType(forIdentifier: .flightsClimbed)
        let stepsCount: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .stepCount)
        let distanceWalkingRunning: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)
        let appleStandTime: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .appleStandTime)
        let appleExerciseTime: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .appleExerciseTime)
        
        let activeEnergyBurned: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)
        let basalEnergyBurned: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .basalEnergyBurned)
        let workouts : HKWorkoutType? = HKObjectType.workoutType()
        
        

        let heightType: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .height)
        let weightType: HKQuantityType? = HKObjectType.quantityType(forIdentifier: .bodyMass)
        let bodyFatPercentage:HKQuantityType? = HKObjectType.quantityType(forIdentifier: .bodyFatPercentage)
        
        
        
        
        let heartRate:HKQuantityType? = HKObjectType.quantityType(forIdentifier: .heartRate)
        let walkingHeartRate:HKQuantityType? = HKObjectType.quantityType(forIdentifier: .walkingHeartRateAverage)
        let restingHeartRate:HKQuantityType? = HKObjectType.quantityType(forIdentifier: .restingHeartRate)
        let heartratevariability:HKQuantityType? = HKObjectType.quantityType(forIdentifier: .heartRateVariabilitySDNN)
        
        let sleepAnalysis: HKCategoryType? = HKObjectType.categoryType(forIdentifier: .sleepAnalysis)
        
        let healthKitTypes: Set = [ stepsCount!,
                                    appleStandTime!,
                                    appleExerciseTime!,
                                    distanceWalkingRunning!,
                                    flightsClimbed!,
                                    workouts!,
                                    activeEnergyBurned!,
                                    bodyFatPercentage!,
                                    heartRate!,
                                    walkingHeartRate!,
                                    restingHeartRate!,
                                    heartratevariability!,
                                    heightType!,
                                    weightType!,
//                                    systolic!,
//                                    dystolic!,
                                    sleepAnalysis!,
//                                    cycling!,
                                    basalEnergyBurned!]
        
        
        
        
        
        // Check for Authorization
        healthStore.requestAuthorization(toShare: nil, read: healthKitTypes) { (bool, error) in
            if (bool) {
                // Authorization Successful
                
                //                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getData), userInfo: nil, repeats: true)
                
                self.getData()
                
            } else {
                return
            }// end if
        } // end of checking authorization
        
    }
    
    @objc func getData() {
        
        var dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        self.getSteps { (result) in
            let step = String(Int(result))
            print(step)
            self.healthdata.steps = step
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        self.getDistance { (result) in
            
            self.healthdata.distance = String(format: "%.2f", Double(result))
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        self.getFlightClimb {(result) in
            self.healthdata.flightclimb = result
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        self.getHeight(completion: {
            (result) in
            self.healthdata.height = result
            dispatchGroup.leave()
        })
        dispatchGroup.enter()
        self.getWeight(completion: {
            (result) in
            self.healthdata.weight = result
            dispatchGroup.leave()
        })
        dispatchGroup.enter()
        self.getBodyFatPercent(completion: {
            (result) in
            self.healthdata.bodyfatpercentge = result
            dispatchGroup.leave()
        })
        dispatchGroup.enter()
        self.getHeartRate {(result) in
            self.healthdata.heartrate = result
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        self.getRestingHeartRate {(result) in
            self.healthdata.restingheartrate = result
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        self.getWalkingHeartRate {(result) in
            self.healthdata.walkingheartrate = result
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        self.getHeartRateVariability(completion: {(result) in
            self.healthdata.heartratevariablity = result
            dispatchGroup.leave()
        })
        
        dispatchGroup.enter()
        self.getActiveEnergy(completion: {(result) in
            self.healthdata.activeEnergy = result
            dispatchGroup.leave()
        })
        dispatchGroup.enter()
        self.retrieveRestingEnergy { (result) in
            self.healthdata.restingEnergy = result
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        self.retrieveExerciseMin(completion: { (result) in
            self.healthdata.exerciseMins = result
            dispatchGroup.leave()
        })
        dispatchGroup.enter()
        self.retrieveStandeMin(completion: { (result) in
            
            self.healthdata.exerciseMins = result
            dispatchGroup.leave()
            
        })
        
        dispatchGroup.enter()
        self.retrieveSleepAnalysis { (result) in
            self.healthdata.sleepAnalysis = result
            dispatchGroup.leave()
        }
        dispatchGroup.enter()
        self.retrieveWorkOut { (result) in
            self.healthdata.workouts = result
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            print(self.healthdata)
            
            healthDataModel = self.healthdata
            
            print("All record Fetch")
//            self.saveHealthData()
            
        }
    }
    
   
    
    func getSteps(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.count())
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    func getDistance(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        let now = Date()
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.meter())
                        resultCount = resultCount/1000
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            } else {
                print("Result Distance Empty")
            }
        }
        
        
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                var resultCount = sum.doubleValue(for: HKUnit.meter())
                resultCount = resultCount/1000
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    func getFlightClimb(completion: @escaping (String) -> Void){
        let type = HKQuantityType.quantityType(forIdentifier: .flightsClimbed)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.count())
                    } // end if
                    
                    DispatchQueue.main.async {
                        completion("\(resultCount) floors")
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion("\(resultCount) floors")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
}


extension FirstInformativeScreenVC {
    
    // MARK: - Activity
    func getActiveEnergy(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = ""
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = "\(sum)"
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                // Return
                DispatchQueue.main.async {
                    completion("\(sum)")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func retrieveRestingEnergy(completion: @escaping (String) -> Void) {
        
        let type = HKQuantityType.quantityType(forIdentifier: .basalEnergyBurned)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = ""
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = "\(sum)"
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                // Return
                DispatchQueue.main.async {
                    completion("\(sum)")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func retrieveExerciseMin(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .appleExerciseTime)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = ""
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = "\(sum)"
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                // Return
                DispatchQueue.main.async {
                    completion("\(sum)")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func retrieveStandeMin(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .appleExerciseTime)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = ""
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = "\(sum)"
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                // Return
                DispatchQueue.main.async {
                    completion("\(sum)")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func retrieveWorkOut(completion: @escaping (String) -> Void) {
        
        let sampleType = HKObjectType.workoutType()
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let query = HKSampleQuery(sampleType: sampleType, predicate: nil, limit: 1, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
            
            if error != nil {
                return
            }
            
            if let result = tmpResult?.first as? HKWorkout
            {
                let seconds = Int(result.duration)
                let str = self.secondsToHoursMinutesSeconds(seconds: seconds)
                self.healthdata.workouts = str
                
                completion(str)
            }
            else
            {
                completion("")
            }
        }
        
        // finally, we execute our query
        healthStore.execute(query)
    }
    
    // MARK: - Body Measurements
    func getHeight(completion: @escaping (String) -> Void){
        
        let heightType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.height)!
        let query = HKSampleQuery(sampleType: heightType, predicate: nil, limit: 1, sortDescriptors: nil) { (query, results, error) in
            if let result = results?.first as? HKQuantitySample{
                
                DispatchQueue.main.async {
                    print("Height => \(result.quantity)")
//                    let heightInMeters = result.quantity.doubleValue(for: HKUnit.meter())
                    completion("\(result.quantity)")
                }
            }else{
                completion("")
                print("OOPS didnt get height \nResults => \(results), error => \(error)")
            }
        }
        
        healthStore.execute(query)
    }
        func getWeight(completion: @escaping (String) -> Void){
            
            let heightType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyMass)!
            let query = HKSampleQuery(sampleType: heightType, predicate: nil, limit: 1, sortDescriptors: nil) { (query, results, error) in
                if let result = results?.first as? HKQuantitySample{
                    
                    DispatchQueue.main.async {
                        print("Weight => \(result.quantity)")
    //                    let heightInMeters = result.quantity.doubleValue(for: HKUnit.meter())
                        completion("\(result.quantity)")
                    }
                }else{
                    completion("")
                }
            }
            
            healthStore.execute(query)
        }
        func getBodyFatPercent(completion: @escaping (String) -> Void){
            
            let heightType = HKSampleType.quantityType(forIdentifier: HKQuantityTypeIdentifier.bodyFatPercentage)!
            
            
            let discreteQuery = HKStatisticsQuery(quantityType: heightType, quantitySamplePredicate: nil,options: [HKStatisticsOptions.discreteAverage,.discreteMin,.discreteMax,.mostRecent]) { (query, results, error) in
                DispatchQueue.main.async {
                    
                    if let result = results?.mostRecentQuantity()
                    {
                        let bodymass = result.doubleValue(for: .percent())
                        completion("\(bodymass * 100) %")
                    }
                    else
                    {
                    completion("")
                    }
                }
            }
            
            healthStore.execute(discreteQuery)
        }
    
    // MARK: - Heart
    
    func getHeartRate(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .heartRate)!
        
        let now = Date()
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.mostRecent],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let beatsPerMinute = statistics.mostRecentQuantity()
                    {
                        resultCount = beatsPerMinute.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute()))
                    }
                    DispatchQueue.main.async {
                        completion("\(resultCount) bpm")
                    }
                }
            }
        }
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let beatsPerMinute = statistics?.mostRecentQuantity() {
                let resultCount = beatsPerMinute.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute()))
                DispatchQueue.main.async {
                    completion("\(resultCount) bpm")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func getRestingHeartRate(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .restingHeartRate)!
        
        let now = Date()
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.mostRecent],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    DispatchQueue.main.async {
                    if let beatsPerMinute = statistics.mostRecentQuantity()
                    {
                        resultCount = beatsPerMinute.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute()))
                    }
                    
                        completion("\(resultCount) bpm")
                    }
                }
            }
        }
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            DispatchQueue.main.async {
            if let beatsPerMinute = statistics?.mostRecentQuantity()
            {
                let resultCount = beatsPerMinute.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute()))
                completion("\(resultCount) bpm")
            }
            
                
            }
        }
        healthStore.execute(query)
    }
    
    func getWalkingHeartRate(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .walkingHeartRateAverage)!
        
        let now = Date()
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.mostRecent],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    DispatchQueue.main.async {
                    if let beatsPerMinute = statistics.mostRecentQuantity()
                    {
                        resultCount = beatsPerMinute.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute()))
                    }
                    
                        completion("\(resultCount) bpm")
                    }
                }
            }
        }
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
                       DispatchQueue.main.async {
            if let beatsPerMinute = statistics?.mostRecentQuantity()
            {
                let resultCount = beatsPerMinute.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute()))
                completion("\(resultCount) bpm")
            }
            
                
            }
        }
        
        healthStore.execute(query)
    }
    
    func getHeartRateVariability(completion: @escaping (String) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .heartRateVariabilitySDNN)!
        
        let now = Date()
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.mostRecent],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    
                    if let resultCount = statistics.mostRecentQuantity() {
                        DispatchQueue.main.async {
                            completion("\(resultCount)")
                        }
                    }
                    else
                    {
                        completion("")
                    }
                }
            }
        }
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let resultCount = statistics?.mostRecentQuantity() {
                DispatchQueue.main.async {
                    completion("\(resultCount)")
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    // MARK: - Sleep
    func retrieveSleepAnalysis(completion: @escaping (String) -> Void) {
        
        // first, we define the object type we want
        if let sleepType = HKObjectType.categoryType(forIdentifier: .sleepAnalysis) {
            
            // Use a sortDescriptor to get the recent data first
//            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            // we create our query with a block completion to execute
                        
            
            let query = HKSampleQuery(sampleType: sleepType, predicate: nil, limit: 1, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                
                if error != nil {
                    
                    // something happened
                    return
                    
                }
                
                if let result = tmpResult?.first as? HKCategorySample
                {
                    let value = (result.value == HKCategoryValueSleepAnalysis.inBed.rawValue) ? "InBed" : "Asleep"
                    self.healthdata.sleepAnalysis = "Healthkit sleep: \(result.startDate) \(result.endDate) - value: \(value)"
                    completion("Healthkit sleep: \(result.startDate) \(result.endDate) - value: \(value)")
                }
                else
                {
                    completion("")
                }
            }
            
            // finally, we execute our query
            healthStore.execute(query)
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        
        let hours = seconds / 3600
        let min = (seconds % 3600) / 60
        let seconds =  (seconds % 3600) % 60
        
        if (hours > 0)
        {
            return "\(hours) hr"
        }
        else if (min >= 0)
        {
            return "\(min) min"
        }
        else
        {
            return "\(seconds) sec"
        }
    }
}



extension Date {
    
    func compareDate(toDate:Date) -> Bool {
        let order = NSCalendar.current.compare(self, to: toDate, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    
}
