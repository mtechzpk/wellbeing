//
//  QuestionVC.swift
//  Well Being
//
//  Created by Waqas on 25/02/2020.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit
import DLRadioButton

class QuestionVC: UIViewController {

    
    @IBOutlet weak var FeelingSlider: UISlider!
    @IBOutlet weak var ProdunctiveSlider: UISlider!
    @IBOutlet weak var SleepSlider: UISlider!
    @IBOutlet weak var StressedSlider: UISlider!
    
    
    @IBOutlet var ques1emojiImgView: UIImageView!
    @IBOutlet var ques2emojiImgView: UIImageView!
    @IBOutlet var ques3emojiImgView: UIImageView!
    @IBOutlet var ques4emojiImgView: UIImageView!
    
    
    @IBOutlet var q1Lbl: UILabel!
    @IBOutlet var q2Lbl: UILabel!
    @IBOutlet var q3Lbl: UILabel!
    @IBOutlet var q4Lbl: UILabel!
    @IBOutlet var q5Lbl: UILabel!
    
    
    
    @IBOutlet var bulletLbl: UILabel!
    
    
    var ans1:String = "5"
    var ans2:String = "5"
    var ans3:String = "5"
    var ans4:String = "5"
    var ans5:String = "5"
    var answerdate:String = ""
    
    
    var animator: UIViewPropertyAnimator!
    
    var iseditMode:Bool = false
    var record:recordData?
//    let healthKitManagerObj = HealthKitManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        getQuestions()
        
        consentformView.isHidden = true
        bulletLbl.text = "• Mr John Welsh (MSc Business Analytics, Imperial College London)\n• Dr Sarah Khavandi (Oxford NHS Foundation School)\n• Dr Laure de Preux (Imperial College Business School, Imperial College London) "
        
    }
    
    @objc func dismissquesView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        if let questions = questionsData
        {
            q1Lbl.text = "1." + questions[0].question!
            q2Lbl.text = "2." + questions[1].question!
            q3Lbl.text = "3." + questions[2].question!
            q4Lbl.text = "4." + questions[3].question!
            
        }
        else
        {
            getQuestions()
        }
        let dateformatter = DateFormatter()
        
//        dateformatter.dateFormat = "YYYY-MM-dd"
//        answerdate = dateformatter.string(from: Date())
        
        if(iseditMode == true && record != nil)
        {
            ans1 = record!.answer1!
            ans2 = record!.answer2!
            ans3 = record!.answer3!
            ans4 = record!.answer4!
            answerdate = record!.answerDate!
            ques1emojiImgView.image = UIImage(named: "Emoji\(record!.answer1!)")
            ques2emojiImgView.image = UIImage(named: "p\(record!.answer2!)")
            ques3emojiImgView.image = UIImage(named: "slept\(record!.answer3!)")
            ques4emojiImgView.image = UIImage(named: "stress\(record!.answer4!)")
            FeelingSlider.value =  (record!.answer1! as NSString).floatValue
            ProdunctiveSlider.value =  (record!.answer2! as NSString).floatValue
            SleepSlider.value =  (record!.answer3! as NSString).floatValue
            StressedSlider.value =  (record!.answer4! as NSString).floatValue
        }
        
    }
    
    
    
    @IBAction func FeelingSlider(_ sender: Any) {
        
        if(self.FeelingSlider.value < 2)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji1")
            ans1 = "1"
        }
        else if(self.FeelingSlider.value >= 2 && self.FeelingSlider.value < 3)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji2")
            ans1 = "2"
        }
        else if(self.FeelingSlider.value >= 3 && self.FeelingSlider.value < 4)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji3")
            ans1 = "3"
        }
        else if(self.FeelingSlider.value >= 4 && self.FeelingSlider.value < 5)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji4")
            ans1 = "4"
        }
        else if(self.FeelingSlider.value >= 5 && self.FeelingSlider.value < 6)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji5")
            ans1 = "5"
        }
        else if(self.FeelingSlider.value >= 6 && self.FeelingSlider.value < 7)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji6")
            ans1 = "6"
        }
        else if(self.FeelingSlider.value >= 7 && self.FeelingSlider.value < 8)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji7")
            ans1 = "7"
        }
        else if(self.FeelingSlider.value >= 8 && self.FeelingSlider.value < 9)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji8")
            ans1 = "8"
        }
        else if(self.FeelingSlider.value >= 9 && self.FeelingSlider.value < 10)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji9")
            ans1 = "9"
        }
        else if(self.FeelingSlider.value >= 10)
        {
            self.ques1emojiImgView.image = UIImage(named: "Emoji10")
            ans1 = "10"
        }
    }
    @IBAction func ProductiveSilder(_ sender: Any) {
//        ProdunctiveSlider.value = roundf(ProdunctiveSlider.value)
        if(self.ProdunctiveSlider.value < 2)
        {
            self.ques2emojiImgView.image = UIImage(named: "p1")
            ans2 = "1"
        }
        else if(self.ProdunctiveSlider.value >= 2 && self.ProdunctiveSlider.value < 3)
        {
            self.ques2emojiImgView.image = UIImage(named: "p2")
            ans2 = "2"
        }
        else if(self.ProdunctiveSlider.value >= 3 && self.ProdunctiveSlider.value < 4)
        {
            self.ques2emojiImgView.image = UIImage(named: "p3")
            ans2 = "3"
        }
        else if(self.ProdunctiveSlider.value >= 4 && self.ProdunctiveSlider.value < 5)
        {
            self.ques2emojiImgView.image = UIImage(named: "p4")
            ans2 = "4"
        }
        else if(self.ProdunctiveSlider.value >= 5 && self.ProdunctiveSlider.value < 6)
        {
            self.ques2emojiImgView.image = UIImage(named: "p5")
            ans2 = "5"
        }
        else if(self.ProdunctiveSlider.value >= 6 && self.ProdunctiveSlider.value < 7)
        {
            self.ques2emojiImgView.image = UIImage(named: "p6")
            ans2 = "6"
        }
        else if(self.ProdunctiveSlider.value >= 7 && self.ProdunctiveSlider.value < 8)
        {
            self.ques2emojiImgView.image = UIImage(named: "p7")
            ans2 = "7"
        }
        else if(self.ProdunctiveSlider.value >= 8 && self.ProdunctiveSlider.value < 9)
        {
            self.ques2emojiImgView.image = UIImage(named: "p8")
            ans2 = "8"
        }
        else if(self.ProdunctiveSlider.value >= 9 && self.ProdunctiveSlider.value < 10)
        {
            self.ques2emojiImgView.image = UIImage(named: "p9")
            ans2 = "9"
        }
        else if(self.ProdunctiveSlider.value >= 10)
        {
            self.ques2emojiImgView.image = UIImage(named: "p10")
            ans2 = "10"
        }
        
        
    }
    @IBAction func SleptSlider(_ sender: Any) {
        SleepSlider.value = roundf(SleepSlider.value)
        if(self.SleepSlider.value < 2)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept1")
            ans3 = "1"
        }
        else if(self.SleepSlider.value >= 2 && self.SleepSlider.value < 3)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept2")
            ans3 = "2"
        }
        else if(self.SleepSlider.value >= 3 && self.SleepSlider.value < 4)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept3")
            ans3 = "3"
        }
        else if(self.SleepSlider.value >= 4 && self.SleepSlider.value < 5)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept4")
            ans3 = "4"
        }
        else if(self.SleepSlider.value >= 5 && self.SleepSlider.value < 6)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept5")
            ans3 = "5"
        }
        else if(self.SleepSlider.value >= 6 && self.SleepSlider.value < 7)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept6")
            ans3 = "6"
        }
        else if(self.SleepSlider.value >= 7 && self.SleepSlider.value < 8)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept7")
            ans3 = "7"
        }
        else if(self.SleepSlider.value >= 8 && self.SleepSlider.value < 9)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept8")
            ans3 = "8"
        }
        else if(self.SleepSlider.value >= 9 && self.SleepSlider.value < 10)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept9")
            ans3 = "9"
        }
        else if(self.SleepSlider.value >= 10)
        {
            self.ques3emojiImgView.image = UIImage(named: "slept10")
            ans3 = "10"
        }
    }
    @IBAction func StressedSlider(_ sender: Any) {
//        StressedSlider.value = roundf(StressedSlider.value)
        if(self.StressedSlider.value < 2)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress1")
            ans4 = "1"
        }
        else if(self.StressedSlider.value >= 2 && self.StressedSlider.value < 3)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress2")
            ans4 = "2"
        }
        else if(self.StressedSlider.value >= 3 && self.StressedSlider.value < 4)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress3")
            ans4 = "3"
        }
        else if(self.StressedSlider.value >= 4 && self.StressedSlider.value < 5)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress4")
            ans4 = "4"
        }
        else if(self.StressedSlider.value >= 5 && self.StressedSlider.value < 6)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress5")
            ans4 = "5"
        }
        else if(self.StressedSlider.value >= 6 && self.StressedSlider.value < 7)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress6")
            ans4 = "6"
        }
        else if(self.StressedSlider.value >= 7 && self.StressedSlider.value < 8)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress7")
            ans4 = "7"
        }
        else if(self.StressedSlider.value >= 8 && self.StressedSlider.value < 9)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress8")
            ans4 = "8"
        }
        else if(self.StressedSlider.value >= 9 && self.StressedSlider.value < 10)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress9")
            ans4 = "9"
        }
        else if(self.StressedSlider.value >= 10)
        {
            self.ques4emojiImgView.image = UIImage(named: "stress10")
            ans4 = "10"
        }
    }
    
    @IBAction func backBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectedBtn_Click(_ sender: DLRadioButton) {
        ans5 = (sender.selected()?.titleLabel?.text!)!
    }
    
    @IBAction func submitBtn_Click(_ sender: Any) {
//
//        if consentformView.isHidden == true
//        {
//            consentformView.isHidden = false
//        }
        self.doSignup()
        if self.iseditMode == false
        {
            self.saveHealthData()
        }
        
    }
    
    func doSignup() {
        
        var parameters = [String:Any]()
        parameters["user_id"] = userData?.id!
        parameters["question1_id"] = questionsData![0].id
        parameters["answer1"] = ans1
        parameters["question2_id"] = questionsData![1].id
        parameters["answer2"] = ans2
        parameters["question3_id"] = questionsData![2].id
        parameters["answer3"] = ans3
        parameters["question4_id"] = questionsData![3].id
        parameters["answer4"] = ans4
        
        let dateformatter = DateFormatter()
        
        dateformatter.dateFormat = "YYYY-MM-dd"
        parameters["answer_date"] = answerdate == "" ? dateformatter.string(from: Date()):answerdate //dateformatter.string(from: Date())
        
        
        
        AFWrapper.requestPOSTURL("submit_answers", params: parameters, success: { (jsonData) in
            
            do{
                let decoder = JSONDecoder()
                let userModel = try decoder.decode(UserModel.self, from: jsonData)
                if userModel.status == 200
                {
                    AlertHelper.showSuccessAlert(WithTitle: "Sucess", Message: userModel.message ?? "Submit Successfully", Sender: self)
                }
                else
                {
                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: userModel.message ?? "Failed to Registered", Sender: self)
                }
            }
            catch
            {
                
            }
            
            
            print(jsonData)
            
        }, failure: { (Error) in
            print(Error.localizedDescription)
        })
    }
    
    func getQuestions() {
        
        AFWrapper.requestGETURL("get_questions", params: nil, success: { (jsonData) in
            do{
                let decoder = JSONDecoder()
                let questionModel = try decoder.decode(QuestionModel.self, from: jsonData)
                if questionModel.status == 200
                {
                    questionsData = questionModel.data
                   if let questions = questionsData
                    {
                        self.q1Lbl.text = "1." + questions[0].question!
                        self.q2Lbl.text = "2." + questions[1].question!
                        self.q3Lbl.text = "3." + questions[2].question!
                        self.q4Lbl.text = "4." + questions[3].question!
                        
                    }
                }
                else
                {
                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: questionModel.message ?? "Failed to getQ", Sender: self)
                }
            }
            catch{
                
            }
        }) { (Error) in
            print(Error.localizedDescription)
        }
        
    }
    func saveHealthData() {
            
            var parameters = [String:Any]()
            parameters["active_energy"] = healthDataModel.activeEnergy
            parameters["body_fat_percentage"] = healthDataModel.bodyfatpercentge
            parameters["exercise_minutes"] = healthDataModel.exerciseMins
            parameters["flights_climbed"] = healthDataModel.flightclimb
            parameters["heart_rate"] = healthDataModel.heartrate
            parameters["heart_rate_variability"] = healthDataModel.heartratevariablity
            parameters["height"] = healthDataModel.height
            parameters["resting_energy"] = healthDataModel.restingEnergy
            parameters["resting_heart_rate"] = healthDataModel.restingheartrate
            parameters["sleep_analysis"] = healthDataModel.sleepAnalysis
            parameters["stand_minutes"] = healthDataModel.standMins
            parameters["steps"] = healthDataModel.steps
            parameters["walking_and_running_distance"] = healthDataModel.distance
            parameters["walking_heart_rate_average"] = healthDataModel.walkingheartrate
            parameters["workout"] = healthDataModel.workouts
            parameters["weight"] = healthDataModel.weight
            parameters["user_id"] = userData?.id
            parameters["latitude"] = healthDataModel.latitude
            parameters["longitude"] = healthDataModel.longitude
            
            AFWrapper.requestPOSTURL("health_access", params: parameters, success: { (jsonData) in
                
                do{
                    let decoder = JSONDecoder()
                    let userModel = try decoder.decode(UserModel.self, from: jsonData)
                    if userModel.status == 200
                    {
                        print("Saved")
    //                    AlertHelper.showSuccessAlert(WithTitle: "Sucess", Message: userModel.message ?? "Submit Successfully", Sender: self)
                    }
                    else
                    {
    //                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: userModel.message ?? "Failed to Registered", Sender: self)
                    }
                }
                catch
                {
                    
                }
                
                
                print(jsonData)
                
            }, failure: { (Error) in
                print(Error.localizedDescription)
            })
        }
    
    // MARK: - Properties
    var ischeckBtn1:Bool = false
    var ischeckBtn2:Bool = false
    var ischeckBtn3:Bool = false
    var ischeckBtn4:Bool = false
    var ischeckBtn5:Bool = false
    var ischeckBtn6:Bool = false
    var ischeckBtn7:Bool = false
    var ischeckBtn8:Bool = false
    var ischeckBtn9:Bool = false
    // MARK: - IBOutlets
    
    @IBOutlet var consentformView: UIView!
    @IBOutlet var checkBtn1: BorderButton!
    @IBOutlet var checkBtn2: BorderButton!
    @IBOutlet var checkBtn3: BorderButton!
    @IBOutlet var checkBtn4: BorderButton!
    @IBOutlet var checkBtn5: BorderButton!
    @IBOutlet var checkBtn6: BorderButton!
    @IBOutlet var checkBtn7: BorderButton!
    @IBOutlet var checkBtn8: BorderButton!
    @IBOutlet var checkBtn9: BorderButton!
    
    // MARK: - Life cycle
    // MARK: - Set up
    // MARK: - IBActions
    @IBAction func checkBtn1_Click(_ sender: BorderButton) {
        if ischeckBtn1 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn1 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn1 = false
        }
    }
    
    @IBAction func checkBtn2_Click(_ sender: BorderButton) {
        if ischeckBtn2 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn2 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn2 = false
        }
    }
    @IBAction func checkBtn3_Click(_ sender: BorderButton) {
        if ischeckBtn3 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn3 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn3 = false
        }
    }
    @IBAction func checkBtn4_Click(_ sender: BorderButton) {
        if ischeckBtn4 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn4 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn4 = false
        }
    }
    @IBAction func checkBtn5_Click(_ sender: BorderButton) {
        if ischeckBtn5 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn5 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn5 = false
        }
    }
    @IBAction func checkBtn6_Click(_ sender: BorderButton) {
        if ischeckBtn6 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn6 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn6 = false
        }
    }
    @IBAction func checkBtn7_Click(_ sender: BorderButton) {
        if ischeckBtn7 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn7 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn7 = false
        }
    }
    @IBAction func checkBtn8_Click(_ sender: BorderButton) {
        if ischeckBtn8 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn8 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn8 = false
        }
    }
    @IBAction func checkBtn9_Click(_ sender: BorderButton) {
        if ischeckBtn9 == false
        {
            sender.setImage(UIImage(named: "tick1"), for: .normal)
            sender.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            ischeckBtn9 = true
        } else {
            sender.setImage(nil, for: .normal)
            ischeckBtn9 = false
        }
    }
    
    @IBAction func submitForm_Click(_ sender: Any) {
//        SaveForm()
    }
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls

    // MARK: - Extensions
}
