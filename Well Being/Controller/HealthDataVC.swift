//
//  HealthDataVC.swift
//  Well Being
//
//  Created by Mahad on 3/19/20.
//  Copyright © 2020 Waqas. All rights reserved.
//

import UIKit

class HealthDataVC: UIViewController {

    // MARK: - Properties
    var datamodal:HealthDataModel = HealthDataModel()
    // MARK: - IBOutlets
    
    @IBOutlet var stepcountLbl: UILabel!
    @IBOutlet var distanceLbl: UILabel!
    @IBOutlet var bpmLbl: UILabel!
    @IBOutlet var flightClimbLbl: UILabel!
    
    @IBOutlet var stepsVw: UIView!
    @IBOutlet var distanceVw: UIView!
    @IBOutlet var heartRateVw: UIView!
    @IBOutlet var flightClimbVw: UIView!
    
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        stepcountLbl.text = datamodal.steps
        distanceLbl.text = datamodal.distance
        bpmLbl.text = datamodal.heartrate
        flightClimbLbl.text = datamodal.flightclimb
        customizeUIView(view: stepsVw)
        customizeUIView(view: distanceVw)
        customizeUIView(view: heartRateVw)
        customizeUIView(view: flightClimbVw)
        
    }
    
    // MARK: - Set up
    
    func customizeUIView(view:UIView) {
//        view.layer.borderWidth=viewContainerBorderWidth
        view.layer.cornerRadius=13.0
    }
    
    
    // MARK: - IBActions
    @IBAction func BackBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    // MARK: - Extensions

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
